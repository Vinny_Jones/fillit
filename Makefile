# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/12/02 17:28:33 by apyvovar          #+#    #+#              #
#    Updated: 2016/12/13 17:51:47 by apyvovar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
FLAGS = -Wall -Wextra -Werror
NAME = fillit
SRC_DIR = src
SRC = main.c input.c input_parse.c solver.c map.c tetriminos_mgmt.c tools.c \
	tools2.c
HEADER_DIR = header

all: $(NAME)

$(NAME):
	$(CC) $(FLAGS) $(addprefix $(SRC_DIR)/, $(SRC)) -I$(HEADER_DIR) -o $(NAME)

clean:

fclean: clean
	/bin/rm -f $(NAME)

re: fclean all
