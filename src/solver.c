/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solver.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/06 16:33:45 by apyvovar          #+#    #+#             */
/*   Updated: 2016/12/13 17:45:38 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	solve(t_elem **tetriminos, int count)
{
	char	**map;
	int		map_side;

	map_side = 1;
	while (map_side * map_side < count * 4)
		map_side++;
	map = create_map(map_side);
	while (!(find_solution(tetriminos, map, map_side)))
	{
		freemap(map, map_side);
		map = create_map(++map_side);
	}
	printmap(map, map_side);
}

int		find_solution(t_elem **tet, char **map, int map_side)
{
	int		skip;

	skip = 0;
	if (!(*tet)->c)
		return (1);
	while (find_space(*tet, map, map_side, skip))
	{
		if (find_solution(++tet, map, map_side))
			return (1);
		else
		{
			tet_delete((*--tet)->c, map, map_side);
			skip++;
		}
	}
	return (0);
}
