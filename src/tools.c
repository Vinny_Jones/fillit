/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/08 11:59:46 by apyvovar          #+#    #+#             */
/*   Updated: 2016/12/08 12:04:58 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

char	*ft_strdup(const char *src)
{
	char	*res;
	int		i;

	res = (char *)malloc(sizeof(char) * ft_strlen(src) + 1);
	if (!res)
		return (NULL);
	i = -1;
	while (src[++i])
		res[i] = src[i];
	res[i] = '\0';
	return (res);
}

size_t	ft_strlen(const char *str)
{
	unsigned int	i;

	i = 0;
	while (*str++)
		i++;
	return (i);
}

char	*ft_strchr(const char *str, int c)
{
	while (*str)
		if (*str == (char)c)
			return ((char *)str);
		else
			str++;
	if (c == '\0')
		return ((char *)str);
	return (NULL);
}

int		ft_isalpha(int c)
{
	if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
		return (1);
	return (0);
}
