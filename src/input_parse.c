/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_parse.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/05 16:12:19 by apyvovar          #+#    #+#             */
/*   Updated: 2016/12/09 12:35:04 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	create_tetriminos(char **input, int nb)
{
	t_elem	*elements[nb];
	t_elem	*last;
	int		i;

	i = 0;
	while (i < nb)
	{
		elements[i] = get_elem(input[i], 'A' + i);
		i++;
	}
	last = NULL;
	elements[i] = add_list(last, -1, -1, 0);
	solve(elements, nb);
}

t_elem	*get_elem(char *input, char c)
{
	t_elem	*new_elem;
	t_elem	*temp;
	int		i;

	new_elem = NULL;
	i = -1;
	while (input[++i])
	{
		if (input[i] == '#')
		{
			if (!new_elem)
			{
				new_elem = add_list(new_elem, i % 5, i / 5, c);
				temp = new_elem;
			}
			else
			{
				temp->next = add_list(new_elem, i % 5, i / 5, c);
				temp = temp->next;
			}
		}
	}
	trim(new_elem);
	return (new_elem);
}

t_elem	*add_list(t_elem *list, int x, int y, char c)
{
	list = (t_elem *)malloc(sizeof(t_elem));
	list->x = x;
	list->y = y;
	list->c = c;
	list->next = NULL;
	return (list);
}

void	trim(t_elem *tetrimino)
{
	t_elem	*tmp;
	int		min;

	tmp = tetrimino;
	min = tmp->x;
	while ((tmp = tmp->next))
		min = MIN(tmp->x, min);
	tmp = tetrimino;
	tmp->x = tmp->x - min;
	while ((tmp = tmp->next))
		tmp->x = tmp->x - min;
	tmp = tetrimino;
	min = tmp->y;
	while ((tmp = tmp->next))
		min = MIN(tmp->y, min);
	tmp = tetrimino;
	tmp->y = tmp->y - min;
	while ((tmp = tmp->next))
		tmp->y = tmp->y - min;
}
