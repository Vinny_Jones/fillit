/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 17:46:06 by apyvovar          #+#    #+#             */
/*   Updated: 2016/12/05 13:06:12 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		main(int argc, const char *argv[])
{
	if (argc == 2)
		input_read(argv[1]);
	else
		ft_putendl("usage:\t./fillit source_file");
	return (0);
}
