/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetriminos_mgmt.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 12:42:32 by apyvovar          #+#    #+#             */
/*   Updated: 2016/12/09 12:58:06 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		find_space(t_elem *tet, char **map, int side, int skip)
{
	int x;
	int y;

	y = 0;
	while (y < side)
	{
		x = 0;
		while (x < side)
		{
			if (isfree(tet, map, x, y))
				if (skip-- == 0)
				{
					install(tet, map, x, y);
					return (1);
				}
			x++;
		}
		y++;
	}
	return (0);
}

void	tet_delete(char c, char **map, int side)
{
	char	*ptr;
	int		i;

	i = 0;
	while (i < side)
	{
		while ((ptr = ft_strchr(map[i], c)))
			*ptr = '.';
		i++;
	}
}

void	install(t_elem *tetrimino, char **map, int x, int y)
{
	t_elem	*temp;

	temp = tetrimino;
	while (temp)
	{
		map[y + temp->y][x + temp->x] = temp->c;
		temp = temp->next;
	}
}

int		isfree(t_elem *tet, char **map, int x, int y)
{
	t_elem	*temp;
	int		side;

	side = mapside(map);
	temp = tet;
	while (temp)
	{
		if (y + temp->y >= side || x + temp->x >= side)
			return (0);
		if (!(map[y + temp->y][x + temp->x] == '.'))
			return (0);
		temp = temp->next;
	}
	return (1);
}
