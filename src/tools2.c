/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/08 12:04:38 by apyvovar          #+#    #+#             */
/*   Updated: 2016/12/08 12:05:04 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	ft_putendl(char const *s)
{
	if (s)
		while (*s)
			ft_putchar(*s++);
	ft_putchar('\n');
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}
