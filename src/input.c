/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/05 13:03:48 by apyvovar          #+#    #+#             */
/*   Updated: 2016/12/14 17:35:53 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"
#include <fcntl.h>

void	input_read(const char *file)
{
	char	*elems[MAXELEMS + 1];
	char	temp[20];
	int		fd;
	ssize_t	read_res;
	int		i;

	if ((fd = open(file, O_RDONLY)) < 0)
		ft_putendl("error");
	i = 0;
	while ((read_res = read(fd, &temp, 20)) == 20 && i < MAXELEMS)
	{
		if (grammar_check(temp) && placement_check(temp))
			elems[i++] = ft_strdup(temp);
		else
			break ;
		if (read(fd, &temp, 1) == 1)
			if (temp[0] != '\n')
				break ;
	}
	elems[i] = NULL;
	if (read_res || i == 0 || temp[0] == '\n')
		ft_putendl("error");
	else
		create_tetriminos(elems, i);
	close(fd);
}

int		grammar_check(char *tetrimino)
{
	int		i;
	int		blocks;

	blocks = 0;
	while (*tetrimino)
	{
		i = 4;
		while (i-- > 0)
			if (*tetrimino == '#' || *tetrimino == '.')
			{
				if (*tetrimino == '#')
					blocks++;
				tetrimino++;
			}
			else
				return (0);
		if (*tetrimino == '\n')
			tetrimino++;
		else
			return (0);
	}
	if (blocks != 4)
		return (0);
	return (1);
}

int		placement_check(char *tetrimino)
{
	int count;

	count = 0;
	while (*tetrimino)
	{
		if (*tetrimino == '#' && *(tetrimino + 1) == '#')
			count++;
		if (*tetrimino == '#' && *(tetrimino + 5) == '#')
			count++;
		tetrimino++;
	}
	if (count > 2)
		return (1);
	return (0);
}
