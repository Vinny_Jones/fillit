/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/08 11:32:23 by apyvovar          #+#    #+#             */
/*   Updated: 2016/12/09 12:56:52 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

char	**create_map(int side)
{
	char	**map;
	int		i;
	int		j;

	i = 0;
	if (!(map = (char **)malloc(sizeof(char *) * side)))
		return (NULL);
	while (i < side)
	{
		if (!(map[i] = (char *)malloc(sizeof(char) * side + 1)))
			return (NULL);
		j = 0;
		while (j < side)
			map[i][j++] = '.';
		map[i][j] = '\n';
		i++;
	}
	return (map);
}

void	freemap(char **map, int map_side)
{
	while (map_side-- > 0)
	{
		free(map[map_side]);
		map[map_side] = NULL;
	}
}

void	printmap(char **map, int map_side)
{
	int i;
	int j;

	i = 0;
	while (i < map_side)
	{
		j = 0;
		while (j < map_side + 1)
		{
			ft_putchar(map[i][j]);
			j++;
		}
		i++;
	}
}

int		mapside(char **map)
{
	int side;

	side = 0;
	while (map[0][side] != '\n')
		side++;
	return (side);
}
