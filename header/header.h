/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 17:35:58 by apyvovar          #+#    #+#             */
/*   Updated: 2016/12/13 17:50:38 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_H
# define HEADER_H
# include <unistd.h>
# include <stdlib.h>
# define MAXELEMS 26
# define ABS(N) (((N) > 0) ? (N) : (N) * -1)
# define MIN(A, B) ((A) < (B) ? (A) : (B))
# define MAX(A, B) ((A) > (B) ? (A) : (B))

typedef struct	s_elem
{
	int				x;
	int				y;
	char			c;
	struct s_elem	*next;
}				t_elem;

void			input_read(const char *file);
int				grammar_check(char *tetrimino);
void			create_tetriminos(char **input, int nb);
int				placement_check(char *tetrimino);
t_elem			*get_elem(char *input, char c);
t_elem			*add_list(t_elem *list, int x, int y, char c);
void			trim(t_elem *tetrimino);
char			*ft_strdup(const char *src);
void			solve(t_elem **tetriminos, int count);
char			**create_map(int size);
int				find_solution(t_elem **tet, char **map, int map_side);
int				find_space(t_elem *tet, char **map, int side, int skip);
int				isfree(t_elem *tet, char **map, int x, int y);
void			install(t_elem *tetrimino, char **map, int x, int y);
void			tet_delete(char c, char **map, int side);
void			freemap(char **map, int map_side);
void			printmap(char **map, int map_side);
int				mapside(char **map);

void			ft_putchar(char c);
size_t			ft_strlen(const char *str);
void			ft_putendl(char const *s);
int				ft_isalpha(int c);
char			*ft_strchr(const char *str, int c);

#endif
